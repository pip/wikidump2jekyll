Convierte un wikidump en articulos Jekyll manteniendo el historial de
revisiones.

```bash
./wikidump2jekyll history.xml wiki/ [ignorar-titulos] [ignorar-revisiones]
```

El primer argumento es el dump en formato xml, por ejemplo el de
[wiki.partidopirata.com.ar](https://archive.org/download/wiki-wikipartidopiratacomar),
el archivo que termina en `-history.xml`.

El segundo argumento es el directorio donde se van a guardar los
articulos.

El tercer argumento es opcional, es un archivo con una lista de nombres
de artículo a ignorar, uno por línea.  Esto es útil para saltearse
artículos atacados por spam.  En nuestro caso el artículo "Cultura
Libre" tiene miles de revisiones todas con spam.

El último argunmento también es opcional, es un archivo con una lista
de numeros de revisiones a ignorar, uno por linea. Quizas a veces nos
interesa no procesar solo algunas revisiones que contienen spam.
